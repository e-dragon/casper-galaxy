/*!
 * casperjs-galaxy - lib/galaxy.js
 *
 *
 * WTFPL Licensed
 */

"use strict";

var require = patchRequire(require);
var utils = require('utils');
var Casper = require('casper').Casper;

var modules = {
    Facebook: require('./modules/FacebookModule').FacebookModule
};


exports.create = function(options) {
    return new CasperGalaxy(options);
};

function CasperGalaxy() {
    CasperGalaxy.super_.apply(this, arguments);
    this.x = require('casper').selectXPath;
    this.facebookModule = new modules.Facebook(this);
    this.result.data = {};
    this.result.errors = [];
}


// must be called here ;)
utils.inherits(CasperGalaxy, Casper);


/**
 * @todo Check if is empty?
 *
 * @param {mixed} data
 */
CasperGalaxy.prototype.saveResultData = function(data) {
    this.result.data = data;
};


/**
 * Render result and exit
 *
 * @param {Boolean} [noExit=false] If is TRUE, script not exits
 * @return {CasperGalaxy}
 */
CasperGalaxy.prototype.renderResult = function(noExit) {
    this.renderJSON(this.result);
    if (!noExit) {
        this.exit();
    }
    return this;
};


/**
 * Provides access to FacebookModule API
 *
 * @return {FacebookModule} Instance of Facebook module
 */
CasperGalaxy.prototype.fb = function() {
    return this.facebookModule;
};


/**
 * Provides access to FacebookModule API
 *
 * @return {FacebookModule} Instance of Facebook module
 */
CasperGalaxy.prototype.facebook = function() {
    return this.facebookModule;
};
