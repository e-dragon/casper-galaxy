/**
 * Facebook Module
 *
 * Provides basic API for work with Facebook.
 * This module extending CasperJS functionality via CasperGalaxy.
 */

/**
 * Create instance of Facebook module
 *
 * @param {CasperGalaxy} context
 */
var FacebookModule = function(context) {
    this.c = context;
};


/**
 * Load page with Facebook login form and submit credentials
 *
 * @param  {string} username
 * @param  {string} password
 * @param  {string} url
 */
FacebookModule.prototype.login = function(username, password, url) {
    if (!url) {
        url = 'https://www.facebook.com/';
    }

    this.c.log("Opening Facebook sign-in page: " + url, "debug");

    this.c.open(url).then(function() {
        this.waitForSelector('#login_form', function() {
            this.log("Login form selector found.", "info");

            if (!(typeof username === 'string' && typeof password === 'string')) {
                this.log("FacebookModule.fillLoginForm(): Fill login and password please.", "error");
            }

            if (this.exists('#login_form')) {
                this.log("facebook.submitLogin(): Login form found.", "info");
            } else {
                this.log("facebook.submitLogin(): Login form not found.", "error");
                return false;
            }

            this.fillSelectors('#login_form', {
                'input[name="email"]': username,
            });

            this.fillSelectors('#login_form', {
                'input[name="pass"]': password,
            });

            this.wait(500, function() {
                this.click('#loginbutton');
                // @todo Random wait?
                this.wait(2000, function() {
                    var url = this.getCurrentUrl();
                    if (~url.indexOf('login.php') || ~url.indexOf('login_attempt')) {
                        this.log('Login failed (URL: "' + url + '").', 'error');
                        this.capture("login-fail.png");
                        return false;
                    } else if (this.exists('#logoutMenu')) {
                        this.log("Successfully logged in.", "info");
                    } else {
                        this.log("Unknown error on Facebook login", "debug");
                    }

                    return true;
                });
            });
        });
    });
};



FacebookModule.prototype.grabGroupInfo = function(casper) {
    console.log("in grabGroupInfo.");
    var links = this.c.evaluate(function() {
        var elements = __utils__.findAll('.fbProfileBrowserListItem a');
        return elements.map(function(e) {

            // old method: vracela jmena z http://facebook.com/<zde-jmeno>
            //return e.getAttribute('href');

            var hoverCard = e.getAttribute('data-hovercard');
            return parseInt(hoverCard.match(/id\=(\d+)/i)[1]);
        });

    });

    // remove duplicates
    return links.sort().filter(function(item, pos, ary) {
        return !pos || item != ary[pos - 1];
    })
};


FacebookModule.prototype.sendMessageById = function fbComposeNewMessage(fbTargetId, message) {
    this.c.thenOpen('https://www.facebook.com/messages/' + fbId + '?fref=ts', function() {
        //this.c.capture("sendMessageById_opened.png");
    });

    this.c.then(function() {
        waiter.wait(this, 3000, function() {
             this.c.sendKeys('textarea', message);
             //this.c.capture("sendMessageById_filled_text.png");
        });
    });

    this.c.then(function() {
        waiter.wait(this, 2000, function() {
            this.c.sendKeys('textarea', this.c.page.event.key.Enter);

            // old methods (mobile device)
            /*
            if (this.c.exists('button[name="Send"]')){
                this.c.click('button[name="Send"]');
            } else if (this.c.exists('button[name="send"]')) {
                this.c.click('button[name="send"]');
            } else if (this.c.exists('input[type="submit"]')) {
                this.c.click('input[type="submit"]');
            }
            */

            this.c.capture("sendMessageById_after_click.png");
        });
    });

    return true;
};


exports.FacebookModule = FacebookModule;
