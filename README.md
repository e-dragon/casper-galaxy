# CasperJS galaxy

:)

## Installation

```bash
npm install "https://e-dragon@bitbucket.org/e-dragon/casper-galaxy.git" --save
```

## Usage

@todo


## API

This project just adding few new methods to original CasperJS api (http://docs.casperjs.org/en/latest/modules/casper.html).
________________________________________________________________________________

### New methods

CasperGalaxy adds the following new features:  

#### Core API (Casper)

- `saveResultData(data)` - Add output data to result variable
- `renderResult()` - Print complete result variable. Structure is as follows:
```
{
    "log": [
        {
            "level": "info",
            "space": "phantom",
            "message": "Hello world",
            "date": "Tue Sep 27 2016 15:59:48 GMT+0200 (CEST)
        }
    ],
    "status": "success",
    "time": 3869,
    "data": {
        "your": "data"
    },
    "errors": []
}
```

#### Facebook (FacebookModule)

...


#### Google (GoogleModule)

...
